@extends('layout')

@section('title', $title)

@section('body')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-8">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../index.php">Homepage</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Tags</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-end">
            <div class="col-3">
                <a href="form.php" type="button" class="btn btn-outline-primary">Add</a>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-8">
                <table class="table table-light">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Title</th>
            <th scope="col">Slug</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($tags as $tag)
        <tr>
            <th scope="row">{{ $tag->id }}</th>
            <td>{{ $tag->title }}</td>
            <td>{{ $tag->slug }}</td>
            <td>
                <a href="form.php?id={{ $tag->id }}">Edit</a>
                <a href="delete.php?id={{ $tag->id }}">Delete</a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
            </div>
        </div>
@endsection
