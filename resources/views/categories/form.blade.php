@extends('layout')

@section('title', $title)

@section('body')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-4">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../index.php">Homepage</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Form</li>
                        <li class="breadcrumb-item"><a href="index.php">Back</a></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-4">
                <form method="post">
                    <div class="mb-3">
                        <label for="title" class="form-label">Title</label>
                        <input type="text" name="title" class="form-control" id="title"
                               @isset($category) value="{{ $category->title }}" @endisset>
                    </div>
                    <div class="mb-3">
                        <label for="slug" class="form-label">Slug</label>
                        <input type="text" name="slug" class="form-control" id="slug"
                               @isset($category) value="{{ $category->slug }}" @endisset>
                    </div>

                    @isset($category)
                        <input type="hidden" name="id" value="{{ $category->id }}">
                    @endisset

                    <input type="submit" class="btn btn-primary" value="Save"/>
                </form>
@endsection
