@extends('layout')

@section('title', $title)

@section('body')
    <div class="container ">
        <div class="row justify-content-center">
            <div class="col-5">
                <div class="list-group">
                    <a href="categories/index.php" class="list-group-item list-group-item-action">Categories</a>
                    <a href="posts/index.php" class="list-group-item list-group-item-action">Posts</a>
                    <a href="/tags/index.php" class="list-group-item list-group-item-action">Tags</a>
                </div>
            </div>
        </div>
    </div>

@endsection